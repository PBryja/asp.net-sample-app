﻿using MvcSiteMapProvider;
using SpodIglyMVC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpodIglyMVC.Infrastructure
{
    public class ProductDetailsDynamicNodeProvider : DynamicNodeProviderBase
    {
        private StoreContext db = new StoreContext();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();

            foreach (var item in db.Albums)
            {
                DynamicNode n = new DynamicNode();
                n.Title = item.AlbumTitle;
                n.Key = "Album_" + item.AlbumId;
                n.ParentKey = "Genre_" + item.GenreId;
                n.RouteValues.Add("id", item.AlbumId);
                returnValue.Add(n);
            }

            return returnValue;
        }
    }
}