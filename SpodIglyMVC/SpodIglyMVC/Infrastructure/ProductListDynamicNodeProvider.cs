﻿using MvcSiteMapProvider;
using SpodIglyMVC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpodIglyMVC.Infrastructure
{
    public class ProductListDynamicNodeProvider : DynamicNodeProviderBase
    {
        private StoreContext db = new StoreContext();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();

            foreach (var item in db.Genres)
            {
                DynamicNode n = new DynamicNode();
                n.Title = item.Name;
                n.Key = "Genre_" + item.GenreId;
                n.RouteValues.Add("genrename", item.Name);
                returnValue.Add(n);
            }

            return returnValue;
        }
    }
}