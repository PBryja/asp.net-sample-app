﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SpodIglyMVC.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        //public string UserId { get; set; }
        [Required(ErrorMessage = "Wprowadz Imie")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Wprowadz Nazwisko")]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Wprowadz Adres")]

        public string Address { get; set; }

        [Required(ErrorMessage = "Wprowadz kod pocztowy i miasto")]
        [StringLength(50)]
        public string CodeAndCity { get; set; }

        [Required(ErrorMessage = "Wprowadz Numer Telefonu")]
        [StringLength(20)]
        [RegularExpression(@"(\+\d{2})*[\d\s-]+", ErrorMessage = "Bledy format numeru telefonu")]
        public string PhoneNumber { get; set; }

        [EmailAddress(ErrorMessage = "Bledny format adresu email")]
        [Required(ErrorMessage = "Wprowadz Email")]
        public string Email { get; set; }


        public string Comment { get; set; }

        public DateTime DateCreated { get; set; }

        public OrderState OrderState { get; set; }

        public decimal TotalPrice { get; set; }

        public List<OrderItem> OrderItems { get; set; }
    }

    public enum OrderState
    {
        [Display(Name = "nowe")]
        New,

        [Display(Name = "wysłane")]
        Shipped
    }
}